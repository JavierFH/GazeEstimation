/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototipo;

import static java.lang.Math.abs;

/**
 *
 * @author Javier
 */
public class EstimacionM {
    double pei,ped,pp;

    public EstimacionM() {
        pei = 10;
        ped = 30;
        pp = 20;
    }
    
    
    
    public void setPosicionEsquinaI(double x){
        pei = x;
    }
    
    public void setPosicionEsquinaD(double x){
        ped = x;
    }
    public void setPosicionPupila(double x){
        pp = x;
    }
    
    public int getEstimacion(){
        double distancia = abs(abs(pei - ped)/2);
        if(distancia >= 100){
            distancia = distancia -10;
        }else{
            if(distancia <= 50){
                distancia = distancia -2;
            }else{
                distancia = distancia -4;
            }
            
            
        }
        
        
        double distanciaI = abs(pei-pp);
        double distanciaD = abs(ped-pp);
        if(distanciaI <= distancia){
            return 1; //izquierda
        }
        if(distanciaD <= distancia){
            return 2; //derecha
        }
        return 0; //enfrente
    }    
}
