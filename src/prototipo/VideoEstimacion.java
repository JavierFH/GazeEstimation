/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototipo;

import javafx.scene.image.Image;
import javafx.util.Duration;
import org.opencv.core.Mat;

/**
 *
 * @author Javier
 */
public class VideoEstimacion {
    Mat ojos;
    Mat frameCam;
    String estimacion;
    Duration timeVideo;
    boolean estado;

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    

    public Duration getTimeVideo() {
        return timeVideo;
    }

    public void setTimeVideo(Duration timeVideo) {
        this.timeVideo = timeVideo;
    }

    public VideoEstimacion() {
        ojos = new Mat();
        frameCam =  new Mat();
        estimacion = "";
        estado = false;
    }
    
    public Mat getOjos() {
        return ojos;
    }

    public void setOjos(Mat ojos) {
        this.ojos = ojos;
    }


    public void setFrameCam(Mat frameCam) {
        this.frameCam = frameCam;
    }

    public String getEstimacion() {
        return estimacion;
    }

    public void setEstimacion(String estimacion) {
        this.estimacion = estimacion;
    }


    public Mat getFrameCam() {
        return frameCam;
    }
    
    
    
}
