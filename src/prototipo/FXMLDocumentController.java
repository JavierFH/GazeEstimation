package prototipo;

import com.mathworks.toolbox.javabuilder.*;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import static org.opencv.core.Core.flip;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;
import org.opencv.imgproc.Imgproc;
import static org.opencv.imgproc.Imgproc.circle;
import static org.opencv.imgproc.Imgproc.rectangle;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;

import prototipo.utils.Utils;
//import sumaJav.Class1;

/**
 *
 * @author Javier
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private ImageView frameActual, ojoIzq, ojoDer;
    @FXML
    private CheckBox checkRostro, checkOjos, checkIris, checkPupila, precision;
    @FXML
    private RadioButton modoCam, modoImagen;
    @FXML
    private Button startButton, anteriorButton, siguienteButton;
    @FXML
    private Text textoMirada;
    @FXML
    private AnchorPane inicioEstimacion, videoP;
    @FXML
    private MediaView videoPlayer, videoPlayerFinal;

    // Objeto de la librería OpenCV que permite capturar video
    private VideoCapture capture;
   VideoWriter video;   
    // Un contador para capturar los frames del video
    private ScheduledExecutorService timer, timerFinal;
    private boolean cameraActive, rostroActivo, ojosActivos, pupilaActivo, irisActivo;
    private static int cameraId = 0;

    // face cascade classifier
    private CascadeClassifier faceCascade;
    private int absoluteFaceSize;

    // Eye cascade classifier
    private CascadeClassifier eyeCascade;
    private int absoluteEyeSize;

    Mat frame = new Mat(); //Imagen tomada de la cámara
    MatOfRect rostro = new MatOfRect();
    MatOfRect rostroAnt = new MatOfRect(); //Permite mejorar la detección del rostro
    MatOfRect ojos = new MatOfRect();
    
    File imgFile;
    /*
      El modo en que funciona el programa
      0 - cámara
      1 - imagen
     */
    int modo = 0;
    MediaPlayer player;
    boolean modoPrecision=true;
    EstimacionM estimacionI = new EstimacionM();
    EstimacionM estimacionD = new EstimacionM();
    Mat imagenOjos = new Mat();
    Runnable resultados;
    ArrayList<VideoEstimacion> frames = new ArrayList<VideoEstimacion>();
   // ArrayList<Mat> framesAux = new ArrayList<Mat>();
    String estimacionObj = "";
    int indice = 0;
    int widthVideo, heightVideo;
    @FXML
    private void mostrarRostro(ActionEvent event) {
        this.rostroActivo = checkRostro.isSelected();
        if (modo == 1) {
            iniciarImagen();
        }

    }
    @FXML
    private void activarPrecision(){
        this.modoPrecision = precision.isSelected();
         if (modo == 1) {
            iniciarImagen();
        }
    }
    @FXML
    private void finalizar(){
        cameraActive = false;
        this.stopAcquisition();
         resultados = null;
        
        inicioEstimacion.setVisible(true);
        videoP.setVisible(false);
        reproducir();
    }
    @FXML
    private void mostrarOjos(ActionEvent event) {
        this.ojosActivos = checkOjos.isSelected();
        if (modo == 1) {
            iniciarImagen();
        }
    }

    @FXML
    private void mostrarIris(ActionEvent event) {
        this.irisActivo = checkIris.isSelected();
        if (modo == 1) {
            iniciarImagen();
        }
    }

    @FXML
    private void mostrarPupila(ActionEvent event) {
        this.pupilaActivo = checkPupila.isSelected();
        if (modo == 1) {
            iniciarImagen();
        }
    }

    @FXML
    private void modoCam(ActionEvent event) {
        this.modo = 0;
        startButton.setText("Iniciar");
        camaraIcono();
    }

    @FXML
    private void modoImagen(ActionEvent event) {
        stopAcquisition();
        this.modo = 1;
        startButton.setText("Seleccionar imagen");
        imagenIcono();
    }

    private void imagenIcono() {
        File file = new File("resources/img/imageFile2.png");
        Image image = new Image(file.toURI().toString());
        frameActual.setImage(image);
    }

    private void camaraIcono() {
        File file = new File("resources/img/webcam.png");
        Image image = new Image(file.toURI().toString());
        frameActual.setImage(image);

    }
    
    private void reproducir(){
        Media media = new Media("file:///D:/Users/Javier/Documents/NetBeansProjects/Prototipo/resources/video/video.mp4");
        player = new MediaPlayer(media);
        player.setAutoPlay(true);
        videoPlayerFinal.setMediaPlayer(player);
        indice = -1;
       /* detectAndDisplay(frames.get(indice).getFrameCam());
        player.seek(frames.get(indice).getTimeVideo());
        player.pause();
        control();*/
       siguiente();
    }
    @FXML
    private void siguiente(){
        System.out.println("siguiente");
        indice++;
  
        //if(indice <= frames.size()-1 ){
        if(frames.get(indice).isEstado() ){
            //mostrar imagen ya procesada
            updateImageView(frameActual, Utils.mat2Image(frames.get(indice).getFrameCam()));
            updateImageView(ojoIzq, Utils.mat2Image(frames.get(indice).getOjos()));
            
            
            textoMirada.setText(frames.get(indice).getEstimacion());
        }else{
            detectAndDisplay(frames.get(indice).getFrameCam());
        }
        player.seek(frames.get(indice).getTimeVideo());
            player.pause();
       // indice++;
        control();
    }
    @FXML
    private void anterior(){
        System.out.println("anterior");
         indice--;
        if(indice <= frames.size()-1){
            //mostrar imagen ya procesada
            System.out.println("Imagen procesada");
            System.out.println("frame guardado");
            updateImageView(frameActual, Utils.mat2Image(frames.get(indice).getFrameCam()));
            System.out.println("ojos guardados");
            updateImageView(ojoIzq, Utils.mat2Image(frames.get(indice).getOjos()));
            
            player.seek(frames.get(indice).getTimeVideo());
            player.pause();
            textoMirada.setText(frames.get(indice).getEstimacion());
        }     
        control();
    }
    
    private void control(){
        System.out.println("Control: "+frames.size() +"  indice: "+indice);
        if(indice == 0){
            anteriorButton.setDisable(true);
        }else{
            anteriorButton.setDisable(false);
        }
        //siguiente
        if(indice >= frames.size()-1){
            siguienteButton.setDisable(true);
        }else{
            siguienteButton.setDisable(false);
        }
        
    }
    @FXML
    private void iniciar(ActionEvent event) {
        switch (modo) {
            case 0:
              // framesAux =  new ArrayList<>();
               frames = new ArrayList<VideoEstimacion>();
               indice = 0;
                inicioEstimacion.setVisible(false);
                videoP.setVisible(true);
                iniciarCamara();
                Media media = new Media("file:///D:/Users/Javier/Documents/NetBeansProjects/Prototipo/resources/video/video.mp4");
                 player = new MediaPlayer(media);
                player.setAutoPlay(true);
                player.setMute(true);
              
                videoPlayer.setMediaPlayer(player);
                widthVideo = videoPlayer.getMediaPlayer().getMedia().getWidth();
                heightVideo = videoPlayer.getMediaPlayer().getMedia().getHeight();
              
               // player.play();
                      
                break;
            case 1:
                seleccionarImagen();
                break;
        }

    }

    private void seleccionarImagen() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Buscar Imagen");

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

        imgFile = fileChooser.showOpenDialog(new Stage());

        iniciarImagen();

    }

    private void iniciarImagen() {
        if (imgFile != null) {
            Image image = new Image("file:" + imgFile.getAbsolutePath());
            frameActual.setImage(image);
            Mat im = Utils.image2Mat(image);

            this.frame = im;
            this.detectAndDisplay(im);
            System.out.println("IniciarImagen()");
            updateImageView(frameActual, Utils.mat2Image(im));
        }
    }

    private void iniciarCamara() {
        if (!this.cameraActive) {
            this.capture.open(cameraId);

            if (this.capture.isOpened()) {
                this.cameraActive = true;

                // Consigue un frame cada 33ms (30 frames /sec) 
                Runnable frameGrabber = new Runnable() {

                    @Override
                    public void run() {
                        Mat frame = grabFrame();    
                        //Image imageToShow = Utils.mat2Image(frame);
                        //updateImageView(frameActual, imageToShow);
                    }
                };
                
                 Mat frame = grabFrame();   
                        //Image imageToShow = Utils.mat2Image(frame);
                        //updateImageView(frameActual, imageToShow);
                this.timer = Executors.newSingleThreadScheduledExecutor();
                this.timer.scheduleAtFixedRate(frameGrabber, 0, 1, TimeUnit.SECONDS);

                this.startButton.setText("Iniciar cámara"); //c
            } else {
                System.err.println("No es posible conectarse con la cámara");
            }
        } else {
            this.cameraActive = false;
            this.startButton.setText("Iniciar");
            // stop the timer
            this.stopAcquisition();
        }
    }

    /**
     * Consigue un frame de la cámara activa
     *
     * @return the {@link Mat} to show
     */
    private Mat grabFrame() {
        frame = new Mat();

        if (this.capture.isOpened()) {
            try {
                this.capture.read(frame);
                
               
                if (!frame.empty()) {

                    // Detección de rostros
                    flip(frame, frame, 1);
                 //   this.detectAndDisplay(frame);
                VideoEstimacion aux = new VideoEstimacion();
                aux.setTimeVideo( player.getCurrentTime());             
                aux.setFrameCam(frame);
                frames.add(aux);
                
                   // framesAux.add(frame);
                    System.out.println("FramesAux size: "+frames.size());
                }
             /*  WritableImage wim = new WritableImage(600, 600);
               player.seek(Duration.millis(500));
               player.pause();
                videoPlayer.snapshot(null,wim);
                ojoDer.setImage(wim);*/


            } catch (Exception e) {
                //  System.err.println("Excepción: " + e);
            }
        }

        return frame;
    }

    /**
     * Detección de rostros
     *
     * @param frame it looks for faces in this frame
     */
    private void detectAndDisplay(Mat frame) {
        if(frame == null){
            System.out.println("error");
        }
        System.out.println("procesando");
        System.out.println("DetectAndDisplay()");
        updateImageView(frameActual, Utils.mat2Image(this.frame));
        ojoIzq.setImage(null);
        this.frame = frame;
        Mat grayFrame = new Mat();

        Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(grayFrame, grayFrame);
        
        // Detectar rostros
        detectFace(grayFrame);
        Rect pos = drawFace(grayFrame);
        if(modoPrecision){
            if(pos == null){ //rostro no detectado
                System.out.println("Rostro no detectado");
                textoMirada.setText("Rostro no detectado");
                VideoEstimacion aux = frames.get(indice);
                aux.setOjos(null);
                aux.setEstimacion("");
                aux.setFrameCam(this.frame);
                aux.setEstado(true);
                int indAux = 1 + indice;

            frames.add(indice,aux);
            frames.remove(indAux);
                return;
            }
           
            Mat rostroDetectado = cropImage(grayFrame, pos.x, pos.y, pos.width, pos.height);
            detectEyes(rostroDetectado);
            drawEyes(pos);
        }else{

            pos = new Rect(0, 0, 0, 0);
            detectEyes(grayFrame);
            drawEyes(pos);
        }
        VideoEstimacion aux = frames.get(indice);
        aux.setEstimacion(estimacionObj);
        aux.setFrameCam(this.frame);
        aux.setEstado(true);
        int indAux = 1 + indice;
        
        frames.add(indice,aux);
        frames.remove(indAux);
        System.out.println("Detect2");
        updateImageView(frameActual, Utils.mat2Image(this.frame));
        updateImageView(ojoIzq, Utils.mat2Image(imagenOjos));
        textoMirada.setText(estimacionObj);
       // video.write(Utils.image2Mat(frameActual.getImage()));
    }

    private void detectFace(Mat grayFrame) {
        this.faceCascade.detectMultiScale(grayFrame, rostro);
    }

    private Rect drawFace(Mat grayFrame) {
        // Se dibuja un rectangulo en la ubicación detectada
        if(rostro.empty()){
            return null;
        }
        Rect[] facesArray = rostro.toArray();

        for (int i = 0; i < facesArray.length; i++) {
            if (rostroActivo) {

                rectangle(this.frame, facesArray[0].tl(), facesArray[0].br(), new Scalar(0, 255, 0), 3);
            }
            return facesArray[i];
            //Imgproc.rectangle(frame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 3);
        }

        
        return null;
    }

    private void detectEyes(Mat grayFrame) {
        this.eyeCascade.detectMultiScale(grayFrame, ojos, 1.1, 2, 0 | Objdetect.CASCADE_SCALE_IMAGE,
                new Size(30, 30), new Size());
    }

    private void drawEyes(Rect pos) {
        System.out.println("inicia drawEyes");
        // Se dibuja un rectangulo en la ubicación detectada
        Rect[] eyesArray = ojos.toArray();
        if (eyesArray == null || eyesArray.length <= 0) {
            return;
        }
        if (eyesArray.length > 0) {
            for (int i = 0; i < eyesArray.length; i++) {

                if (ojosActivos) {
                    Rect aux = eyesArray[i];
                    //System.out.println(aux);
                    aux.x = pos.x + eyesArray[i].x;
                    aux.y = pos.y + eyesArray[i].y;
                    aux.width = eyesArray[i].width;
                    aux.height = eyesArray[i].height;
                    rectangle(frame, aux.tl(), aux.br(), new Scalar(233, 79, 40), 3);
                    //  rectangle(frame, eyesArray[i].tl(), eyesArray[i].br(), new Scalar(233, 79, 40), 3);
                }
                eyesArray[i].x = eyesArray[i].x+3;
                eyesArray[i].y = eyesArray[i].y+3;
                eyesArray[i].width = eyesArray[i].width-6;
                eyesArray[i].height = eyesArray[i].height-6;
                
            
                Mat image = cropImage(frame, eyesArray[i].x, eyesArray[i].y, eyesArray[i].width, eyesArray[i].height);
                //Rect rectCrop = new Rect(eyeArray[i].x, eyeArray[i].y, eyeArray[i].width, eyeArray[i].height);
                //Mat image = new Mat(frame,rectCrop);
                //--  updateImageView(ojoIzq, Utils.mat2Image(image));

                int aux = (int) eyesArray[i].width / 3;
                int aux2 = (int) (aux + aux - aux / 8);
                
                Mat ojoIzquierdo = cropImage(image, 0, 0, aux, eyesArray[i].height);
             
                Mat ojoDerecho = cropImage(image, aux2, 0, aux, eyesArray[i].height);
                estimacionI = new EstimacionM();
                estimacionI = new EstimacionM();
                boolean band1 = detectIris(ojoIzquierdo,0);
                boolean band2 = detectIris(ojoDerecho,1);
               System.out.println("finalizado detección de iris");
                if(band1 && band2){
                     estimarMirada();
                }else{
                    textoMirada.setText(null);
                    estimacionObj = "";
                }
                //  updateImageView(ojoDer, Utils.mat2Image(ojoDerecho));
                if (ojosActivos || pupilaActivo || irisActivo) {
                   // updateImageView(ojoIzq, Utils.mat2Image(image));
                } else {
                    ojoIzq.setImage(null);
                }
                imagenOjos = image;

            }
        } else {
            ojoIzq.setImage(null);
            estimacionObj = "";
            imagenOjos = new Mat();
        }
        System.out.println("termina draw Eyes");

    }

    private Mat cropImage(Mat frame, int x, int y, int width, int height) {
        System.out.println("recorte imagen");
        try {
            Rect rectCrop = new Rect(x, y, width, height);
            return new Mat(frame, rectCrop);
        } catch (Exception e) {
            System.out.println("Excepcion recorte imagen: "+e);
        }
        System.out.println("Finaliza metodo recorte imagen ");
        return null;

    }

    private boolean detectIris(Mat frame,int element) {
        System.out.println("Inicia deteccion de iris");
         Mat circles = new Mat();
        Mat src_gray = new Mat();
        Imgproc.cvtColor(frame, src_gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(src_gray, src_gray);
        try {
                     
             imwrite( "C:/IOM/ojo.JPG", frame );

        PupilaIris.PI pi = new PupilaIris.PI();
        Object[] result = new Object[3];
            try {
                      result = pi.buscarPupilaSinBW(1);
                 //  result = pi.buscarPupilaConBW(1);
            } catch (MWException e) {
                System.out.println("error pupila: "+e);
                return false;
            }
            
  
         pi.dispose();
      
         double n1 = ((MWNumericArray)result[0]).getDouble(1);
         double n2 = ((MWNumericArray)result[0]).getDouble(2);
         int radius = (int) Math.round(((MWNumericArray)result[0]).getDouble(3));
         
            
             Point pt = new Point(Math.round(n1), Math.round(n2));
             
                double[] aux = detectCorners(frame);
                if(element == 0){
                    estimacionI.setPosicionPupila(n1);
                    estimacionI.setPosicionEsquinaI(aux[0]);
                    estimacionI.setPosicionEsquinaD(aux[1]);
                    
                }else{
                    estimacionD.setPosicionPupila(n1);
                    estimacionD.setPosicionEsquinaI(aux[0]);
                    estimacionD.setPosicionEsquinaD(aux[1]);
                }
               if (irisActivo) {
                    circle(frame, pt, radius, new Scalar(0, 255, 0), 2); //iris
                }
                if (pupilaActivo) {
                   circle(frame, pt, 3, new Scalar(0, 0, 255), 2); //pupila
                }
                     System.out.println("finaliza deteccion de iris");
                 return true;
        } catch (MWException e) {
            System.out.println("Falla :C "+e);
            return false;
        }

    }
    
    private void estimarMirada(){
        int ojoIzqM = estimacionI.getEstimacion();
        int ojoDerM = estimacionD.getEstimacion();
        
        if(ojoIzqM == 1 && ojoDerM == 1){
            estimacionObj = "Mirada hacia la izquierda";
            //textoMirada.setText();
            return;
        }
        if(ojoIzqM == 2 && ojoDerM == 2){
            estimacionObj = "Mirada hacia la derecha";
           // textoMirada.setText("Mirada hacia la derecha");
            return;
        }
       estimacionObj = "Mirada enfrente";
           // textoMirada.setText("Mirada enfrente");
        
    }

    private double[] detectCorners(Mat frame) {
        double[] aux = new double[2];
        aux[0] = frame.rows() / 2;
        aux[1] = frame.cols() - 3.5;

        circle(frame, new Point(6, aux[0]), 1, new Scalar(0, 0, 255), 2, 8, 0);
        circle(frame, new Point(aux[1], frame.rows() / 2), 1, new Scalar(0, 0, 255), 2, 8, 0);
        aux[0] = 6;
        return aux;
    }

    /**
     * Stop the acquisition from the camera and release all the resources
     */
    private void stopAcquisition() {
        if (this.timer != null && !this.timer.isShutdown()) {
            try {
                this.timer.shutdown();
                this.timer.awaitTermination(500, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
            }
        }

        if (this.capture.isOpened()) {
            this.capture.release();
        }

    }
    

    /**
     * Update the {@link ImageView} in the JavaFX main thread
     *
     * @param view the {@link ImageView} to update
     * @param image the {@link Image} to show
     */
    private void updateImageView(ImageView view, Image image) {
        Utils.onFXThread(view.imageProperty(), image);
    }

    /**
     * On application close, stop the acquisition from the camera
     */
    protected void setClosed() {
        this.stopAcquisition();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicioEstimacion.setVisible(true);
        videoP.setVisible(false);
        modoCam.setSelected(true);
        modo = 0;
        camaraIcono();
        this.cameraActive = false;
        this.modoPrecision = true;
        this.rostroActivo = checkRostro.isSelected();;
        this.ojosActivos = checkOjos.isSelected();;
        this.pupilaActivo = checkPupila.isSelected();;
        this.irisActivo = checkIris.isSelected();;
        capture = new VideoCapture();
        this.faceCascade = new CascadeClassifier();
        this.absoluteFaceSize = 0;
        this.eyeCascade = new CascadeClassifier();
        this.absoluteEyeSize = 0;
        // load the classifier(s)
       // boolean load1 = this.faceCascade.load("Resources/haarcascades/faceDetection3.xml");
       boolean load1 = this.faceCascade.load("Resources/haarcascades/haarcascade_frontalface_alt.xml");
        //boolean load2 = this.eyeCascade.load("Resources/haarcascades/eyeDetection1.xml");
        boolean load2 = this.eyeCascade.load("Resources/haarcascades/haarcascade_mcs_eyepair_small.xml");
        
        System.out.println("load: " + load1 + "\nload2: " + load2);

    }
}
